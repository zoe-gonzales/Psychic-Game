
var win = 0;
var loss = 0;
var guessesLeft = 9;
var userGuesses = [];
var computerChoices = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

// global function to reset game
function resetGuesses () {
    this.guessesLeft = 9;
    this.userGuesses = [];
}
// Detects user's key input
document.onkeyup = function(event) {
// Saves user's key input to a var
var userGuess = event.key;
var computerGuess = computerChoices[Math.floor(Math.random() * computerChoices.length)];

// adds user guess to "Your guesses so far"
var Try = {
    guessMade: function (userGuess) {
        if (userGuess.match(/^[a-zA-Z]+$/)) {
            if (userGuess === computerGuess) {
                win++;
                resetGuesses();
            } else {
                // adds incorrect guesses to array
                userGuesses.push(userGuess);
                // guesses left go down by one
                guessesLeft--;
            }
            // executes if user runs out of guesses
            if (guessesLeft === 0 && userGuess !== computerGuess) {
                loss++;
                resetGuesses();
            }
        }
    }
}
// Calls guessMade function
Try.guessMade(userGuess);

// Connects user view to variables
document.getElementById("win-text").innerText = "Wins: " + win;
document.getElementById("loss-text").innerText = "Losses: " + loss;
document.getElementById("guesses-left").innerText = "Guesses Left: " + guessesLeft;
document.getElementById("user-guesses").innerText = "Your Guesses so far: " + userGuesses.join(" ");

}
